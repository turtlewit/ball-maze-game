extends Camera

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var target : Spatial setget targetSet
var wr_target

func targetSet(val):
	target = val
	wr_target = weakref(target)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if wr_target.get_ref():
		translation.x = lerp(translation.x, target.global_transform.origin.x, 1 * delta)
