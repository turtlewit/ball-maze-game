extends Camera

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var ball : RigidBody setget ball_set 
var wr_ball

func ball_set(val):
	ball = val
	wr_ball = weakref(ball)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if wr_ball.get_ref():
		translation.x = lerp(translation.x, ball.global_transform.origin.x, 1 * delta)
