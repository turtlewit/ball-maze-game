extends OptionButton

export(String, DIR) var levelDirectoryPath 
var levelDirectory

var levels = []
var buttons = []

class ListEntry:
	var name
	var file

func _ready():
	levelDirectory = Directory.new()
	if levelDirectory.open(levelDirectoryPath) == OK:
		levelDirectory.list_dir_begin()
		print("Levels in directory:")
		var fileName = levelDirectory.get_next()
		while (fileName != ""):
			if not levelDirectory.current_is_dir():
				print("\t" + fileName)
				levels.append(fileName)
			fileName = levelDirectory.get_next()
	else:
		print("ERR: Cannot open level directory!")
	
	for level in levels:
		var button = ListEntry.new()
		button.name = level.get_basename()
		button.file = levelDirectoryPath + "/" + level
		buttons.append(button)
		add_item(button.name, buttons.size() - 1)

func _on_Button_button_down():
	get_tree().change_scene(buttons[get_selected_id()].file)
