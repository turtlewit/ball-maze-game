extends Node

# Ball Spawn Position
export(NodePath) var ballSpawn
var spawnPosition = Vector3(0, 0, 0)

# Ball Prefab File
export(PackedScene) var ballPrefab 

# Required Paths
# These nodes must be placed in-editor and will not be spawned
export(NodePath) var playerInstancePath
export(NodePath) var goalInstanceParentPath
export(NodePath) var boundsInstancePath

# Instance variables for nodes
var ballInstance
var playerInstance
var goalInstances = []
var goalInstanceParent
var boundsInstance
var holes

# We have to be careful when we want to check if the ball is in bounds
# This variable makes sure we only check when we want to
var checkBounds = false

func init():
	# Get nodes from instance paths
	var spawn = get_node(ballSpawn)
	spawnPosition = spawn.translation

	playerInstance = get_node(playerInstancePath)
	goalInstanceParent = get_node(goalInstanceParentPath)
	for node in goalInstanceParent.get_children():
		goalInstances.append(node)
	boundsInstance = get_node(boundsInstancePath)

	# We can find the "Holes" instance from the player instance
	for child in playerInstance.get_children():
		if child.name == "Holes":
			holes = child

	# Spawn the ball
	spawnBall()

func _process(delta):
	# We only want to check collision when the player is active
	# This checks to see if the player's state is "STATE_IN_GAME"
	if Input.is_action_just_pressed("quit"):
		quit()
	if playerInstance.state == playerInstance.getEnumStateInGame():
		checkCollisions()

func checkCollisions():
	# Check all hole instances for collision
	var overlapping_bodies = []
	for hole in holes.get_children():
		overlapping_bodies += hole.get_overlapping_bodies()
	var collision = false
	for body in overlapping_bodies:
		if body.name == ballInstance.name:
			collision = true
	if collision:
		resetLevel()

	# Check if the player is in bounds
	var inBounds = false
	if checkBounds:
		for body in boundsInstance.get_overlapping_bodies():
			if body.name == ballInstance.name:
				inBounds = true
		if not inBounds:
			resetLevel()
	else:
		for body in boundsInstance.get_overlapping_bodies():
			if body.name == ballInstance.name:
				inBounds = true
		if inBounds:
			checkBounds = true

	# Check if the player is colliding with the goal
	for goalInstance in goalInstances:
		collision = false
		for body in goalInstance.get_overlapping_bodies():
			if body.name == ballInstance.name:
				collision = true
		if collision:
			winLevel(goalInstance)

func spawnBall():
	ballInstance = ballPrefab.instance() # Spawn the ball, set the instance to ballInstance
	ballInstance.set_name("Ball")
	add_child(ballInstance) # Make the ball instance a child of this, the "Scene" node
	ballInstance.translation = spawnPosition # Reposition the ball

func resetLevel():
	checkBounds = false
	ballInstance.queue_free()
	playerInstance.reset()

func quit():
	get_tree().quit()

# overrides
func winLevel(instance):
	pass
