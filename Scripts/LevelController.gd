extends RigidBody

# Accel is how fast the turn rate accelerates
# Max speed caps the angular velocity
export var accel = 5
export var maxSpeed = 10

# Velocity variables
var vel = {
		x = 0,
		y = 0,
		z = 0}
var inp = {
		up    = 0,
		right = 0,
		tilt  = 0}

# Original transform, so we can get back to it when the level resets
var originalTransform

# STATE_IN_GAME
# Player has control, normal gameplay
# STATE_NO_INPUT
# Game is running, but node does not check for input
# STATE_RESET
# Node resets back to start position, then spawns a new ball and returns to STATE_IN_GAME
# STATE_SPAWN
# Behaviour when first spawned.
enum {
		STATE_IN_GAME, 
		STATE_NO_INPUT, 
		STATE_RESET, 
		STATE_SPAWN,
		STATE_DESPAWN}
var stateFunctions = {
		STATE_IN_GAME:  funcref(self, "handleInput"),
		STATE_NO_INPUT: funcref(self, "noInput"),
		STATE_RESET:    funcref(self, "doReset"),
		STATE_SPAWN:    funcref(self, "spawn"),
		STATE_DESPAWN:  funcref(self, "despawn")}
var state = STATE_SPAWN

# Scene node
var scene

func _ready():
	# We are controlling a physics node
	set_physics_process(true)
	scene = get_node("/root/Scene")
	originalTransform = get_transform()

func _physics_process(delta):
	stateFunctions[state].call_func(delta)

# Called by the physics engine
func _integrate_forces(state):
	state.set_angular_velocity(Vector3(vel.x, vel.y, vel.z))

func _input(event):
	inputUpdate()

func inputReset():
	inp.up = 0
	inp.right = 0
	inp.tilt = 0

func inputUpdate():
	inp.up    = Input.get_action_strength("tilt_up") - Input.get_action_strength("tilt_down")
	inp.right = Input.get_action_strength("tilt_right") - Input.get_action_strength("tilt_left")
	inp.tilt  = Input.get_action_strength("rotate_right") - Input.get_action_strength("rotate_left")

func noInput(delta):
	pass

func handleInput(delta):
	if inp.up != 0:
		vel.x = accelerate(vel.x, -inp.up, delta)
	else:
		vel.x = decelerate(vel.x, delta)
	
	if inp.right != 0:
		vel.z = accelerate(vel.z, -inp.right, delta)
	else:
		vel.z = decelerate(vel.z, delta)

	if inp.tilt != 0:
		vel.y = accelerate(vel.y, -inp.tilt, delta)
	else:
		vel.y = decelerate(vel.y, delta)

func accelerate(num, negative, delta):
	# num: number to accelerate
	# negative: do we want to accelerate in the other direction? 1 or -1. Other values result in unknown behaviour

	# Clamp to max speed
	if (num + (accel * delta * negative) > maxSpeed) or ((num + (accel * delta * negative)) < -maxSpeed):
		return maxSpeed * negative

	# Return accelerated value
	return num + (accel * delta * negative) 

func decelerate(num, delta):
	# num: number to decelerate
	var num_abs = abs(num)

	# clamp to 0
	if (num_abs - accel * delta) <= 0:
		return 0;

	# negative determines if we should return a negative value
	var negative = num / num_abs

	# return decelerated value
	return (num_abs - accel * delta) * negative

func setState(stateEnum):
	state = stateEnum

func reset():
	resetVelocity()
	translation = Vector3(0, 0, 0)
	state = STATE_RESET

func resetVelocity():
	vel.x = 0
	vel.y = 0
	vel.z = 0

func doReset(delta):
	# Spherical Linear Interpolate between current rotation and target (originTransform) rotation. 
	set_transform(
			Transform( 												# We want to make a new transform from the interpolated rotation
				Quat(transform.basis).slerp( 			# Create a Quat from the current transform, interpolate it
					Quat(originalTransform.basis), 	# Target is the original transform
					4 * delta)  										# speed is 400% per second 
				#transform.origin)) 							# Keep our transform's origin
				))
	if rotation_degrees.distance_to(Vector3(0,0,0)) <= 0.5:
		set_transform(originalTransform)
		scene.spawnBall()
		state = STATE_IN_GAME

func spawn(delta):


func despawn(delta):


func getEnumStateNoInput():
	return STATE_NO_INPUT

func getEnumStateInGame():
	return STATE_IN_GAME
