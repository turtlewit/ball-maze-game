extends "res://Scripts/SceneBase.gd"

export(NodePath) var sceneSpawnPath
onready var sceneSpawn = get_node(sceneSpawnPath)

export(PackedScene) var initialScene
var currentScene : Node
export(NodePath) var cameraPath
onready var camera = get_node(cameraPath)

func _ready():
	instanceScene(initialScene)

func _process():
	if playerInstance.state == playerInstance.getEnumStateInGame():
		checkCollisions()

func instanceScene(sceneToInstance):
	currentScene = sceneToInstance.instance()
	add_child(currentScene)
	

func winLevel(instance):
	currentScene.state = currentScene.STATE_DESPAWN
	instanceScene(instance.targetScene)

