extends "res://Scripts/SceneBase.gd"

export(PackedScene) var mainMenu
export(NodePath) var cameraPath

var cameraInstance

func _ready():
	.init()
	cameraInstance = get_node(cameraPath)
	cameraInstance.ball = ballInstance


func winLevel():
	get_tree().change_scene_to(mainMenu)

func spawnBall():
	.spawnBall()
	if cameraInstance:
		cameraInstance.ball = ballInstance

func quit():
	get_tree().change_scene_to(mainMenu)
